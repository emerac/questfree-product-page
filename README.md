# questfree product page

## About

This is a product page that could be used for a business. It is
written solely in HTML and CSS.

[Click here](https://emerac.gitlab.io/questfree-product-page) to
view this page!

This page generally follows the requirements as laid out in
freeCodeCamp's Responsive Web Design Course, Project 3. However,
I chose not to follow the requirements exactly so that I could
exercise more creativity.

All images are used in accordance with their respective licenses.

## Disclaimer

The business and product(s) portrayed are fictitious. No identification
with an actual business, product(s), or website design is intended or
should be inferred. Any resemblance is purely coincidental.

## License

This project is licensed under the GNU General Public License.
For the full license text, view the [LICENSE](LICENSE) file.

Copyright © 2021 emerac
